package com.numberreader.fileoperation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest
class DeleteFileTest {

    @Autowired
    private DeleteFile deleteFile;

    @Value("${path.delete}")
    private String path;

    @BeforeEach
    void save() throws IOException {
        File file = new File(path + "abc.jpg");
        file.createNewFile();
    }

    @Test
    void delete() {
        deleteFile.delete();
        File file = new File(path);
        assertThat(file.listFiles().length, equalTo(0));
    }
}