package com.numberreader.javacv;

import lombok.extern.slf4j.Slf4j;
import org.bytedeco.opencv.opencv_core.*;
import org.bytedeco.opencv.opencv_objdetect.CascadeClassifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.opencv.global.opencv_imgcodecs.imread;
import static org.bytedeco.opencv.global.opencv_imgcodecs.imwrite;
import static org.bytedeco.opencv.global.opencv_imgproc.*;
import static org.bytedeco.opencv.global.opencv_objdetect.CASCADE_SCALE_IMAGE;

@Slf4j
@Service
public class JavaCVImageResolver {

    @Value("${path.image}")
    private String image;

    @Value("${path.cropImage}")
    private String cropImagePath;

    @Value("${file.tmp.image}")
    private String tmpFile;

    public void imageResolver() {
        ClassPathResource xmlResource = new ClassPathResource("haarcascade/haarcascade_russian_plate_number_16.xml");
        Path imgFile = Paths.get(image + tmpFile);

        String xmlFile = null;
        try {
            xmlFile = xmlResource.getFile().getPath();
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        Mat img = imread(imgFile.toString());

        Mat img2 = new Mat();
        cvtColor(img, img2, COLOR_BGR2RGB);

        CascadeClassifier cascadeClassifier = new CascadeClassifier(xmlFile);
        RectVector rectVector = new RectVector();
        cascadeClassifier.detectMultiScale(img2, rectVector, 1.1, 5,
                CASCADE_SCALE_IMAGE, new Size(220, 60), new Size(220, 60));

        recognitionImage(imgFile, img2, rectVector);

        cascadeClassifier.close();
    }

    private void recognitionImage(Path imgFile, Mat src, RectVector rectVector) {

        List<Rect> rectCrops = new ArrayList<>();
        for (Rect rect : rectVector.get()) {
            rectangle(src, new Point(rect.x(), rect.y()), new Point(rect.x() + rect.width(), rect.y() + rect.height()),
                    new Scalar(0, 0, 255, 128), 3, 8, 0);
            rectCrops.add(new Rect(rect.x(), rect.y(), rect.width(), rect.height()));
        }

        imwrite(image + imgFile.getFileName().toString(), src);

        for (int i = 0; i < rectCrops.size(); i++) {
            Mat rectImage = new Mat(src, rectCrops.get(i));
            Mat resize = new Mat();
            resize(rectImage, resize, new Size(0, 0), 3.500, 3.500, INTER_AREA);
            imwrite(cropImagePath + "cropImage" + i + ".jpg", rectImage);
        }
    }
}
