package com.numberreader.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.zip.DataFormatException;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler({IOException.class, DataFormatException.class, URISyntaxException.class})
    public String errorControllerResolver() {
        return "error";
    }

}
