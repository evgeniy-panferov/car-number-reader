package com.numberreader.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@AllArgsConstructor
public class ImageController {

    private final ImageFacade imageFacade;

    @GetMapping
    public String getGreeting() {
        return "main-page";
    }

    @PostMapping(value = "/image", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String getImage(@RequestParam("car-number") MultipartFile file) {
        log.info("Файл {} загружен ", file.getOriginalFilename());
        imageFacade.imageResolveStart(file);
        return "finish";
    }

}
