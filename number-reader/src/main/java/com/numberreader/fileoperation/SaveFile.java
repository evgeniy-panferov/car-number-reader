package com.numberreader.fileoperation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Component
public class SaveFile {

    @Value("${path.save}")
    private String image;

    @Value("${file.tmp.image}")
    private String tmpFile;

    public void save(MultipartFile multipartFile) {
        String originalFilename = multipartFile.getOriginalFilename();
        log.info("Сохранение файла началось - {}", originalFilename);
        Path filepath = Paths.get(image, tmpFile);
        try (OutputStream os = Files.newOutputStream(filepath)) {
            os.write(multipartFile.getBytes());
            log.info("Сохранение файла закончилось - {}", originalFilename);
        } catch (IOException e) {
            log.error(e.getMessage());
        }

    }
}
