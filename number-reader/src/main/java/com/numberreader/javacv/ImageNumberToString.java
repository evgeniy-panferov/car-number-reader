package com.numberreader.javacv;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.leptonica.PIX;
import org.bytedeco.tesseract.TessBaseAPI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.leptonica.global.lept.pixDestroy;
import static org.bytedeco.leptonica.global.lept.pixRead;

@Slf4j
@Service
@RequiredArgsConstructor
public class ImageNumberToString {

    private BytePointer bytePointer;

    @Value("${path.cropImage}")
    private String path;

    @Value("${tessaract.path}")
    private String tessaractPath;

    public void imageToString() {

        try (TessBaseAPI tessBaseAPI = new TessBaseAPI()) {

            if (tessBaseAPI.Init(tessaractPath, "rus") != 0) {
                log.error("Could not initialize tesseract.");
                System.exit(1);
            }

            File file = new File(this.path);

            List<String> strings = new ArrayList<>();
            PIX image = null;
            if (file.listFiles().length != 0) {
                for (File file1 : file.listFiles()) {
                    image = pixRead(file1.getAbsolutePath());
                    tessBaseAPI.SetImage(image);
                    bytePointer = tessBaseAPI.GetUTF8Text();
                    String string = bytePointer.getString();
                    strings.add(string);
                }
                bytePointer.deallocate();
                pixDestroy(image);
            }
        }
    }
}
