package com.numberreader.fileoperation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.util.stream.Stream;

@Slf4j
@Component
public class DeleteFile {

    @Value("${path.delete}")
    private String deletePath;

    public void delete() {
        File path = new File(deletePath);
        File[] files = path.listFiles();
        if (files != null && files.length > 0) {
            Stream.of(files).forEach(file -> {
                log.info("Файл - {} удаляется", file.getName());
                FileSystemUtils.deleteRecursively(file);
            });
        }
    }
}
