package com.numberreader.fileoperation;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

@SpringBootTest
class SaveFileTest {

    @Autowired
    private SaveFile saveFile;

    @Test
    void save() {
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", "MockFile.jpg", MediaType.IMAGE_JPEG_VALUE, "abc".getBytes());
        saveFile.save(mockMultipartFile);
    }
}