package com.numberreader.javacv;

import com.numberreader.fileoperation.DeleteFile;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

@SpringBootTest
class JavaCVImageResolverTest {

    @Autowired
    private JavaCVImageResolver javaCVImageResolver;

    @Autowired
    private DeleteFile deleteFile;

    @Value("${path.cropImage}")
    private String path;

    @AfterEach
    @BeforeEach
    void delete() {
        deleteFile.delete();
    }

    @Test
    void imageResolver() {
        javaCVImageResolver.imageResolver();
        File file = new File(path);
        assertThat(file.listFiles().length, greaterThan(0));
    }
}