package com.numberreader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class NumberReaderApplication {
    public static void main(String[] args) {
        SpringApplication.run(NumberReaderApplication.class);
    }
}
