package com.numberreader.controller;

import com.numberreader.fileoperation.DeleteFile;
import com.numberreader.fileoperation.SaveFile;
import com.numberreader.javacv.JavaCVImageResolver;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@SpringBootTest
@AutoConfigureWireMock(port = 0)
class ImageControllerTest {

    @Autowired
    private ImageController imageController;

    @MockBean
    private SaveFile saveFile;
    @MockBean
    private DeleteFile deleteFile;
    @MockBean
    private JavaCVImageResolver javaCVImageResolver;

    @Value("${image.url}")
    private String url;

    @Test
    void getImageWiremock() throws IOException {

        stubFor(post(urlEqualTo("/image"))
                .willReturn(aResponse()
                        .withHeader("Content-type", "text/html")
                        .withStatus(200)
                        .withBody("finish")));

        Response post = RestAssured.given()
                .contentType("text/html")
                .post(url + "/image");

        assertEquals("finish", post.getBody().asString());


    }

    @Test
    void getImageMockito() throws IOException {

        MockMultipartFile mockMultipartFile =
                new MockMultipartFile("file", "MockFile.jpg", MediaType.IMAGE_JPEG_VALUE, "abc".getBytes());

        imageController.getImage(mockMultipartFile);

        verify(saveFile, Mockito.times(1))
                .save(mockMultipartFile);

        verify(deleteFile, Mockito.times(1))
                .delete();

        verify(javaCVImageResolver, Mockito.times(1))
                .imageResolver();
    }

    @Test
    void getGreeting() throws IOException {

        stubFor(get(urlEqualTo("/"))
                .willReturn(aResponse()
                        .withHeader("Content-type", "text/html")
                        .withStatus(200)
                        .withBody("main")));

        Response post = RestAssured.given()
                .contentType("text/html")
                .get(url + "/");

        assertEquals("main", post.getBody().asString());

    }
}

