package com.carcheckout;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicController {

    @Value("${app.service.name}")
    private String serviceName;

    @GetMapping
    public String getGreeting() {
        return "Hello from " + serviceName;
    }
}
