package com.numberreader.controller;

import com.numberreader.fileoperation.DeleteFile;
import com.numberreader.fileoperation.SaveFile;
import com.numberreader.javacv.ImageNumberToString;
import com.numberreader.javacv.JavaCVImageResolver;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
@AllArgsConstructor
public class ImageFacade {

    private final SaveFile saveFile;
    private final DeleteFile deleteFile;
    private final JavaCVImageResolver javaCVImageResolver;
    private final ImageNumberToString imageNumberToString;

    public void imageResolveStart(MultipartFile file) {
        saveFile.save(file);
        javaCVImageResolver.imageResolver();
        imageNumberToString.imageToString();
        deleteFile.delete();
    }
}
